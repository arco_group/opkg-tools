#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

# Copyright 2015 - Oscar Aceña

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import sys
import tarfile
import shutil
from commodity.pattern import Bunch


def chown_root(info):
    info.uid = info.gid = 0
    info.uname = info.gname = "root"
    return info


def set_executable(info):
    info.mode = int("0111101101", 2)
    return info


def root_exec(info):
    set_executable(info)
    chown_root(info)
    return info


class ControlFile:
    tarball_name = "control.tar.gz"

    def __init__(self, control_file):
        self.cwd = os.path.dirname(control_file)
        self.control_file = control_file
        self.fields = Bunch()

        self.parse_control_file()

    def parse_control_file(self):
        with file(self.control_file) as src:
            while True:
                line = src.readline()
                if not line:
                    break

                try:
                    key, value = map(str.strip, line.split(":"))
                    self.fields[key] = value
                except ValueError:
                    pass

    def build(self, tempdir):
        self.tarball_path = os.path.join(tempdir, self.tarball_name)
        with tarfile.open(self.tarball_path, "w:gz") as tar:
            tar.add(self.control_file, "control", filter=chown_root)
            self.add_scripts(tar)

    def add_scripts(self, tar):
        for script in ["preinst", "postinst", "prerm", "postrm"]:
            path = os.path.join(self.cwd, script)
            if os.path.isfile(path):
                tar.add(path, script, filter=root_exec)


class InstallFile:
    tarball_name = "data.tar.gz"

    def __init__(self, control, install_file):
        self.control = control
        self.install_file = install_file

    def build(self, tempdir):
        self.tarball_path = os.path.join(tempdir, self.tarball_name)
        with tarfile.open(self.tarball_path, "w:gz") as tar:
            for path, name in self.get_file_list():
                if not os.path.exists(path):
                    print "Warning: missing file '{0}'".format(path)
                    continue

                tar.add(path, name, filter=chown_root)
            self.add_init_script(tar)

    def get_file_list(self):
        retval = []
        if not os.path.isfile(self.install_file):
            return retval

        with file(self.install_file) as src:
            for line in src:
                line = line.strip()
                if not line:
                    continue

                if line.startswith("#"):
                    continue

                path, name = map(str.strip, line.split())
                if name.endswith("/"):
                    name = os.path.join(name, os.path.basename(path))
                retval.append((path, name))

        return retval

    def add_init_script(self, tar):
        src = "opkg/init"
        if not os.path.isfile(src):
            return
        dst = "etc/init.d/{0}".format(self.control.fields.Package)
        tar.add(src, dst, filter=root_exec)


class OpkPackage:
    def __init__(self, control, data):
        self.control = control
        self.data = data

    def build(self, tempdir):
        self.create_debian_binary_file(tempdir)

        fields = self.control.fields
        pkg_file = "{0}_{1}.ipk".format(fields.Package, fields.Version)
        with tarfile.open(pkg_file, "w:gz") as tar:
            for path in [self.debian_binary_file,
                         self.control.tarball_path,
                         self.data.tarball_path]:
                name = os.path.basename(path)
                tar.add(path, name, filter=chown_root)

    def create_debian_binary_file(self, dirname):
        self.debian_binary_file = os.path.join(dirname, "debian-binary")
        with file(self.debian_binary_file, "w") as dst:
            dst.write("2.0\n")


class OpkgBuilder:
    opkg_dir = "opkg"
    control_file = "{0}/control".format(opkg_dir)
    install_file = "{0}/install".format(opkg_dir)
    temp_dir = "{0}/OPKG".format(opkg_dir)

    def __init__(self):
        self.check_dir(self.opkg_dir)
        self.check_file(self.control_file)
        self.create_temp_dir()

        self.control = ControlFile(self.control_file)
        self.install = InstallFile(self.control, self.install_file)
        self.package = OpkPackage(self.control, self.install)

    def run(self):
        self.control.build(self.temp_dir)
        self.install.build(self.temp_dir)
        self.package.build(self.temp_dir)

    def check_dir(self, name):
        if not os.path.isdir(name):
            print "Error, missing 'opkg' directory"
            sys.exit(-1)

    def check_file(self, name):
        if not os.path.isfile(name):
            print "Error, missing 'opkg/control' file"
            sys.exit(-1)

    def create_temp_dir(self):
        if os.path.exists(self.temp_dir):
            shutil.rmtree(self.temp_dir)

        os.mkdir(self.temp_dir)


if __name__ == "__main__":
    OpkgBuilder().run()
